// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import "~/assets/styles.scss";
import DefaultLayout from "~/layouts/Default.vue";

import { library } from "@fortawesome/fontawesome-svg-core";
import { faGitlab, faLinkedinIn } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faExternalLinkAlt, faFile } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

export default function (Vue, { router, head, isClient }) {
  head.meta.push({
    name: "viewport",
    content: "width=device-width,initial-scale=1",
  });

  Vue.component("Layout", DefaultLayout);

  library.add(faGitlab, faLinkedinIn, faEnvelope, faExternalLinkAlt, faFile);
  Vue.component("FontAwesomeIcon", FontAwesomeIcon);
}
