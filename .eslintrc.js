module.exports = {
  env: {
    node: true,
    es6: true,
  },
  parser: "vue-eslint-parser",
  plugins: ["gridsome"],
  extends: [
    "plugin:vue/recommended",
    "eslint:recommended",
    "plugin:prettier/recommended",
    "prettier/vue",
  ],
  rules: {
    "prettier/prettier": "warn",
    "gridsome/format-query-block": "error",
  },
};
