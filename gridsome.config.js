// This is where project configuration and plugin options are located.
// Learn more: https://gridsome.org/docs/config

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

module.exports = {
  siteName: "Patrick Benter",
  siteUrl: "https://www.patrickbenter.com",
  siteDescription: "Hi, I'm Patrick Benter. This is my portfolio",
  outputDir: "public",
  plugins: [
    {
      use: "@gridsome/source-strapi",
      options: {
        apiURL: process.env.GRIDSOME_SERVER_URL,
        singleTypes: ["footer", "portfolio"],
      },
    },
  ],
};
