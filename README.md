# My personal website portfolio

Built using Vue atomic design and GraphQL, with Strapi CMS content integration. Static site generated with Gridsome.
