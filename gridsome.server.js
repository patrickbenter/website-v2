// Server API makes it possible to hook into various parts of Gridsome
// on server-side and add custom data to the GraphQL data layer.
// Learn more: https://gridsome.org/docs/server-api/

// Changes here require a server restart.
// To restart press CTRL + C in terminal and run `gridsome develop`

const http = require("http");
const fs = require("fs");

module.exports = function (api) {
  api.loadSource(({ addCollection }) => {
    // Use the Data Store API here: https://gridsome.org/docs/data-store-api/
  });

  api.createPages(({ createPage }) => {
    // Use the Pages API here: https://gridsome.org/docs/pages-api/
  });

  api.onCreateNode((options, collection) => {
    // Pull all images from Strapi into static folder
    let results = findAllByKey(options, "image");
    results.forEach((image) => {
      if (image != null) {
        const filePath = `./static/${image.hash + image.ext}`;

        http.get(
          `${process.env.GRIDSOME_SERVER_URL}${image.url}`,
          (response) => {
            const file = fs.createWriteStream(filePath);
            response.pipe(file);
          }
        );
      }
    });
  });

  function findAllByKey(obj, keyToFind) {
    return Object.entries(obj).reduce(
      (acc, [key, value]) =>
        key === keyToFind
          ? acc.concat(value)
          : typeof value === "object" && value
          ? acc.concat(findAllByKey(value, keyToFind))
          : acc,
      []
    );
  }
};
